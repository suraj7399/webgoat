package org.owasp.server;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.owasp.server")
public class ParentConfig {

}
